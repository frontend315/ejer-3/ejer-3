import CalculatorModel from "../models/CalculatorModel";
import Multiply from "../controllers/Multiply";
import Divide from "../controllers/Divide";
import Add from "../controllers/Add";
import Substract from "../controllers/Sustract";


type operation = '+' | '-' | '*' | '/';

const getValuesOfOperation= (): {operators:Array<string>, operatorSelected: string} =>{
    const operationsValids:Array<operation>= ['+','-','*','/']
    const expression = process.argv.filter(prss => prss.includes('OP='))[0];
    const operation = expression.split('=')[1]
    
    let operators:string[] = []
    let operatorSelected:operation = '+'
    
    operationsValids.forEach( operator => {
        if( operation.includes(operator)){
            operators = operation.split(operator);
            operatorSelected = operator;
        }
    })

    return {operators , operatorSelected}
}

const calc = () => {
    const {operators, operatorSelected} = getValuesOfOperation();
    const [
        firstoperator,
        secondoperator
    ] = operators

    let anwser: CalculatorModel = new CalculatorModel(0,0);

    switch (operatorSelected){
        case '*':
            anwser = new Multiply(parseFloat(firstoperator), parseFloat(secondoperator))
            break;
        case '/':
            anwser = new Divide(parseFloat(firstoperator), parseFloat(secondoperator))
            break;
        case '+':
            anwser = new Add(parseFloat(firstoperator), parseFloat(secondoperator))
            break;
        case '-':
            anwser = new Substract(parseFloat(firstoperator), parseFloat(secondoperator))
            break;
    }

    return anwser.calculation();
}

console.log(calc())