import Calculator from "../interfaces/Calcutator";

class CalculatorModel implements Calculator {
    private first_operator: number;
    private second_operator: number;

    constructor(first:number, second:number){
        this.first_operator = first;
        this.second_operator = second;
    }

    calculation(): number | number[] {
        return [this.first_operator, this.second_operator]
    }

    setFirstOperator(firstOperator: number): void {
        this.first_operator = firstOperator
    }

    setSecondOperator(secondOperator: number): void {
        this.second_operator = secondOperator
    }  

}

export default CalculatorModel;