import CalculatorModel from "../models/CalculatorModel";

class Multiply extends CalculatorModel {
    
    constructor(firstOperator:number, secondOperator:number){
        super (firstOperator, secondOperator)
    }

    calculation(): number | number[] {
        const value = super.calculation()
        const [first, second] = typeof value === 'object' ? value : [0,0]
        return first * second;
    }
}

export default Multiply;