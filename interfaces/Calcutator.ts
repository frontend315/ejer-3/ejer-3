interface Calculator {
    calculation(): number | number[];
    setFirstOperator(firstOperator: number): void
    setSecondOperator(secondOperator: number): void
}

export default Calculator;